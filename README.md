Instant Answer
==============

Bla bla bla bla bla bla bla.

Description
-----------

This library accesses the [DuckDuckGo Instant Answer API](https://duckduckgo.com/api), and returns a JSON or XML result.

Usage
-----

An example:

```ruby
require "instant_answer"

answer = InstantAnswer.query("Tango")

answer["Entity"]         #=> "music genre"
answer["AbstractSource"] #=> "Wikipedia",
answer["AbstractURL"]    #=> "https://en.wikipedia.org/wiki/Tango",
answer["AbstractText"]   #=> "Tango is a partner dance that originated in the 1890s along the Ro de la Plata, the natural border between Argentina and Uruguay, and soon spread to the rest of the world."
```

Installation
------------

```
$ gem install instant_answer
```

Alternatives:
-------------

* https://github.com/andrewrjones/ruby-duck-duck-go

* https://github.com/tmlee/duck_duck_go_api

