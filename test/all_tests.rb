test "JSON format" do
  stub_get("https://api.duckduckgo.com/?format=json&no_html=1&skip_disambig=1&q=Tango&no_redirect=1", 'default.json')

  result = InstantAnswer.query("Tango")

  assert_equal "https://en.wikipedia.org/wiki/Tango", result["AbstractURL"]
  assert_equal "music genre", result["Entity"]
end

test "XML format" do
  stub_get("https://api.duckduckgo.com/?format=xml&no_html=1&skip_disambig=1&q=Tango&no_redirect=1", 'default.xml')

  result = InstantAnswer.query("Tango", format: 'xml')
  xml = Nokogiri::XML(result)

  assert_equal "https://en.wikipedia.org/wiki/Tango", xml.at("AbstractURL").text
  assert_equal "music genre", xml.at("Entity").text
end

test "no_redirect" do
  stub_get("https://api.duckduckgo.com/?format=json&no_html=1&skip_disambig=1&q=%21imdb+Inception&no_redirect=1", 'no_redirect.json')

  result = InstantAnswer.query("!imdb Inception", no_redirect: 0) # ignored

  assert_equal "http://www.imdb.com/find?s=all&q=Inception", result["Redirect"]
end

test "skip_disambig" do
  stub_get("https://api.duckduckgo.com/?format=json&no_html=1&skip_disambig=1&q=apple&no_redirect=1", 'skip_disambig_enabled.json')
  stub_get("https://api.duckduckgo.com/?format=json&no_html=1&skip_disambig=0&q=apple&no_redirect=1", 'skip_disambig_disabled.json')

  result = InstantAnswer.query("apple")
  assert_equal "A", result["Type"]
  assert_equal 8, result["RelatedTopics"].size
  assert !result["AbstractText"].empty?

  result = InstantAnswer.query("apple", skip_disambig: 0)
  assert_equal "D", result["Type"]  
  assert_equal 43, result["RelatedTopics"].size
  assert result["AbstractText"].empty?
end
