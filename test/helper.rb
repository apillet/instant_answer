require_relative "../lib/instant_answer"

require "fakeweb"
require "nokogiri"

FakeWeb.allow_net_connect = false

def fixture_file(filename)
  file_path = File.expand_path(File.join(File.dirname(__FILE__), 'fixtures', filename))

  File.read(file_path)
end

def stub_get(url, filename)
  FakeWeb.register_uri(:get, url, { body: fixture_file(filename) })
end
