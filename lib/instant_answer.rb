require 'requests/sugar'

class InstantAnswer

  VERSION = '0.0.1'

  URL = "https://api.duckduckgo.com".freeze

  DEFAULTS = {
    format: 'json',
    no_html: 1,    
    skip_disambig: 1
  }

  def self.query text, options = {}
    params = DEFAULTS.merge(options).merge(q: text, no_redirect: 1)

    response = Requests.get(URL, params: params)

    if params[:format] == 'json'
      response.json
    else
      response.body
    end
  end
end
